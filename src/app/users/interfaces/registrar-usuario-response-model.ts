export interface RegistrarUsuarioResponseModel {
    name:string,
    job:string,
    id:string,
    createAt: Date
}
