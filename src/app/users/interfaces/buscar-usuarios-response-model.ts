import { BuscarUsuarioResponseModel } from "./buscar-usuario-response-model"

export interface BuscarUsuariosResponseModel{
    page:string,
    per_page:number,
    total:number,
    total_pages:number,
    data: BuscarUsuarioResponseModel [],
    support: {
        url:string,
        text:string
    }
}