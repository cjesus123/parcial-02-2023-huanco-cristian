import {Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {UsersService} from "../../services/users.service";
import {MatSnackBar} from "@angular/material/snack-bar";
import { BuscarUsuarioResponseModel } from '../../interfaces/buscar-usuario-response-model';
import { BuscarUsuariosResponseModel } from '../../interfaces/buscar-usuarios-response-model';

@Component({
  selector: 'app-listado',
  templateUrl: './listado.component.html',
  styleUrls: ['./listado.component.css']
})
export class ListadoComponent implements OnInit {

  displayedColumns: string[] = ['id', 'email','first_name','last_name','avatar'];
  usuarios: BuscarUsuarioResponseModel[] = [];

  constructor(private router: Router,
              private userService: UsersService,
              private matSnackBar: MatSnackBar) {
  }

  ngOnInit(): void {
    this.userService.buscarUsuarios().subscribe({next:(resp:BuscarUsuariosResponseModel)=>{
      this.usuarios = resp.data;
      if(resp.data.length == 0){
        this.showSnackBar("La lista de usuarios se encuentra vacia"); 
      }
    },error:(error)=>{
      this.showSnackBar("No se puede obtener la lista de usuarios");
    }})
  }

  addData() {
    this.router.navigate(['/users/user']);
  }

  showSnackBar(mensaje: string) {
    this.matSnackBar.open(mensaje, 'ACEPTAR', {
      duration: 3000
    });
  }
}
