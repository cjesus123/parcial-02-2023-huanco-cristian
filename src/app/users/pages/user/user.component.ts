import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {MatSnackBar} from "@angular/material/snack-bar";
import {UsersService} from "../../services/users.service";
import {Router} from "@angular/router";
import { RegistrarUsuarioRequestModel } from '../../interfaces/registrar-usuario-request-model';
import { RegistrarUsuarioResponseModel } from '../../interfaces/registrar-usuario-response-model';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

  miFormulario: FormGroup = this.fb.group({
    name: [undefined,[Validators.required,Validators.minLength(3)]],
    job:  [undefined,[Validators.required,Validators.minLength(3)]] 
  });

  constructor(private fb: FormBuilder,
              private matSnackBar: MatSnackBar,
              private userService: UsersService,
              private router: Router) {
  }

  ngOnInit(): void {
    this.miFormulario.reset({});
    this.miFormulario.untouched;
  }

  guardar(){
    if(this.miFormulario.invalid){
      this.miFormulario.markAsTouched;
      return;
    }

    const usuario:RegistrarUsuarioRequestModel = {
      name: this.miFormulario.value.name,
      job: this.miFormulario.value.job
    }
    this.userService.registrarUsuario(usuario).subscribe({next: (resp:RegistrarUsuarioResponseModel)=>{
      this.showSnackBar("Se registro el usuario con el ID " + resp.id);
      this.router.navigate(['/users/listado']);
    },error: (error)=>{
      this.showSnackBar("No se pudo registrar el usuario");
      console.error(error);
    }});
  }

  cancelar() {
    this.router.navigate(['/users/listado']);
  }

  showSnackBar(mensaje: string) {
    this.matSnackBar.open(mensaje, 'ACEPTAR', {
      duration: 3000
    });
  }
}
