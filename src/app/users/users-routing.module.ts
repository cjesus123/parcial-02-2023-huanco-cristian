import {NgModule} from '@angular/core';
import {RouterModule, Routes} from "@angular/router";
import {PrincipalComponent} from "./pages/principal/principal.component";
import {ListadoComponent} from "./pages/listado/listado.component";
import {UserComponent} from "./pages/user/user.component";

const routes: Routes = [
  {
    path: '',
    component: PrincipalComponent,
    children: [
      {
        path: 'listado',
        component: ListadoComponent
      },
      {
        path: 'user',
        component: UserComponent
      },
      {
        path: '**',
        redirectTo: 'listado'
      }
    ]
  }
];

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class UsersRoutingModule {
}
