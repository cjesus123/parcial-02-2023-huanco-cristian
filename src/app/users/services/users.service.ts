import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import { RegistrarUsuarioRequestModel } from '../interfaces/registrar-usuario-request-model';
import { RegistrarUsuarioResponseModel } from '../interfaces/registrar-usuario-response-model';
import { Observable } from 'rxjs';
import { BuscarUsuariosResponseModel } from '../interfaces/buscar-usuarios-response-model';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  url:string = 'https://reqres.in/api';

  constructor(private httpClient: HttpClient) {
  }

  registrarUsuario(usuario:RegistrarUsuarioRequestModel):Observable<RegistrarUsuarioResponseModel>{
    return this.httpClient.post<RegistrarUsuarioResponseModel>(this.url + '/users',usuario); 
  }

  buscarUsuarios():Observable<BuscarUsuariosResponseModel>{
    return this.httpClient.get<BuscarUsuariosResponseModel>(this.url + '/users?page=1');
  }
}
