# Examen Parcial N° 2 - Programación III

### Ing. y Lic. en Sistemas - 2023

### Objetivos

- Implementar Frontend que consuma los endpoints indicados

### Tiempo

- 2 horas reloj

### Evaluacion

- Se evaluará la versión del proyecto en el repositorio correspondiente, a la hora de finalización del examen, estimado
  en 2hs de duración
- El proyecto debe compilar sin errores en cualquier entorno de programación en el que se abra
- Se probará la funcionalidad desde el Frontend

### Punto de partida

- Se proveerá el esquema de Frontend parcialmente desarrollado, de manera que el alumno pueda lograr los objetivos en el
  tiempo previsto.

## Consigna

#### Módulo Usuarios

_Se desea implementar un frontend para un microservicio que permita consultar y registrar usuarios._

#### Restricciones:

- Todos los atributos del usuario son obligatorios y deben tener un min de 3 caracteres.

#### Funcionalidad

- Crear Usuario
  - Endpoint: POST https://reqres.in/api/users
  - RequestBody:
    ```json
    {
      "name": "morpheus",
      "job": "developer"
    }
    ```

- Buscar Usuarios
  - Endpoint: GET https://reqres.in/api/users?page=1
  - ResponseBody:
    ```json
    {
    "page": 1,
    "per_page": 6,
    "total": 12,
    "total_pages": 2,
    "data": [
    {
    "id": 1,
    "email": "george.bluth@reqres.in",
    "first_name": "George",
    "last_name": "Bluth",
    "avatar": "https://reqres.in/img/faces/1-image.jpg"
    },
    {
    "id": 2,
    "email": "janet.weaver@reqres.in",
    "first_name": "Janet",
    "last_name": "Weaver",
    "avatar": "https://reqres.in/img/faces/2-image.jpg"
    },
    {
    "id": 3,
    "email": "emma.wong@reqres.in",
    "first_name": "Emma",
    "last_name": "Wong",
    "avatar": "https://reqres.in/img/faces/3-image.jpg"
    },
    {
    "id": 4,
    "email": "eve.holt@reqres.in",
    "first_name": "Eve",
    "last_name": "Holt",
    "avatar": "https://reqres.in/img/faces/4-image.jpg"
    },
    {
    "id": 5,
    "email": "charles.morris@reqres.in",
    "first_name": "Charles",
    "last_name": "Morris",
    "avatar": "https://reqres.in/img/faces/5-image.jpg"
    },
    {
    "id": 6,
    "email": "tracey.ramos@reqres.in",
    "first_name": "Tracey",
    "last_name": "Ramos",
    "avatar": "https://reqres.in/img/faces/6-image.jpg"
    }
    ],
    "support": {
    "url": "https://reqres.in/#support-heading",
    "text": "To keep ReqRes free, contributions towards server costs are appreciated!"
    }
    }
    ```

